# cypress-cucumber-boilerplate

## Overview
Ce repo donne une structure de base pour créer ses premiers tests Cypress en utilisant le format Cucumber dans un environnement GitLab CI.

Les packages de base pour créer, exécuter, et générer des rapports de tests sont prédéfinis dans le `package.json`. Installez l'environnement avec la commande:

```shell
npm install
```

Vous pouvez dès à présent lancer les tests avec:
```
$(npm bin)/cypress run
```

Les tests sont définis dans le répertoire `cypress/e2e`, les actions words Gherkin sont définis dans `cypress/support/step_definitions`.

Pour les utilisateurs de VSCode, il est fortement recommandé d'installer l'extension [Cucumber (Gherkin) Full Support](https://marketplace.visualstudio.com/items?itemName=alexkrechik.cucumberautocomplete) (la configuration de celui-ci est fournie pour ce projet).

## Pipeline
Le pipeline défini par le fichier `gitlab-ci.yml` est constitué de 3 stages:
  - **✅ run tests**: exécution des scripts de test en suivant un template commun.
  - **📝 create reports**: merge des fichiers de résultats générés par les jobs de test du stage précédent, et génération de la page HTML au format mochawesome (accessible à l'adresse `${CI_PAGES_URL}/mochawesome.html`).
  - **📣 push results**: envoie des résultats au run CucumberStudio (variable `${CUCUMBER_JSON_PUSH_URL}` à mettre à jour dans le fichier yaml). 

## F.A.Q.
...
