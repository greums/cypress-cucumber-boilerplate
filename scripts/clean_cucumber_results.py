import json
import os
from collections import defaultdict

TEST_RESULTS_FILE = os.getenv("JSON_RESULTS_FILE", "merged-test-results.json")
OUTPUT_FILE = os.getenv("OUTPUT_RESULTS_FILE", "cleaned-test-results.json")


def filter_skipped_scenarii(scenarii):

  cleaned_scenarii = []

  if len(scenarii) > 1:
    for scenario in scenarii:
      results = [
        step["result"]["status"]
        for step in scenario["steps"]
      ]

      if not all([status == "skipped" for status in results]):
        cleaned_scenarii.append(scenario)
  else:
    cleaned_scenarii.extend(scenarii)

  return cleaned_scenarii


def filter_duplicated_scenarii(scenarii):
  return [
    scenarii[-1]
    for scenarii in scenarii_to_scenarii_by_id(scenarii).values()
  ]


def scenarii_to_scenarii_by_id(scenarii):
  scenarii_dict = defaultdict(list)
  for scenario in scenarii:
    scenarii_dict[scenario["id"]].append(scenario)
  return scenarii_dict


def clean_duplicated_scenarii(scenarii):

  cleaned_scenarii = []
  for scenarii in scenarii_to_scenarii_by_id(scenarii).values():
    cleaned_scenarii.extend(filter_skipped_scenarii(scenarii))

  return filter_duplicated_scenarii(cleaned_scenarii)


def merge_duplicated_features(features):

  all_scenarii = []
  for feature in features:
    all_scenarii.extend(feature["elements"])

  cleaned_feature = features[0].copy()
  cleaned_feature["elements"] = clean_duplicated_scenarii(all_scenarii)

  return cleaned_feature


def read_results_file():

  features_dict = defaultdict(list)

  with open(TEST_RESULTS_FILE) as f:
    for feature in json.load(f):
      features_dict[feature["id"]].append(feature)

  return features_dict


def clean_results(features_dict):

  cleaned_results = []

  for features in features_dict.values():

    if len(features) > 1:
      cleaned_results.append(merge_duplicated_features(features))
    else:
      cleaned_results.extend(features)

  return cleaned_results


def write_cleaned_results_file(results):
  with open(OUTPUT_FILE, "w") as f:
    f.write(json.dumps(results, indent=4, sort_keys=True, ensure_ascii=False))


def main():
  write_cleaned_results_file(clean_results(read_results_file()))


if __name__ == "__main__":
  main()
