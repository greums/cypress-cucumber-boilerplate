stages:
  - ✅ run tests
  - 📝 create reports
  - 📣 push results

variables:
  TEST_IMAGE: cypress/browsers:node16.14.2-slim-chrome100-ff99-edge
  CYPRESS_NUM_TESTS_KEPT_IN_MEMORY: 0
  CUCUMBER_REPORTS_PATH: reports/cucumber-json
  CUCUMBER_JSON_PUSH_URL: https://studio.cucumber.io/import_test_reports/xxxx/yyyy/cucumber-json
  JSON_RESULTS_FILE: results.json
  JUNIT_REPORTS_PATH: reports/mocha
  JUNIT_RESULTS_FILE: results.xml
  MOCHAWESOME_REPORTS_PATH: reports/mochawesome

default:
  retry:
    max: 1
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
      - job_execution_timeout

.test:
  image: $TEST_IMAGE
  when: always
  dependencies: []
  timeout: 10 minutes
  cache:
    key:
      files:
        - package-lock.json
    paths:
      - .npm/
  artifacts:
    when: always
    name: "$CI_COMMIT_REF_SLUG"
    paths:
      - cypress/screenshots
      - cypress/videos
      - ${MOCHAWESOME_REPORTS_PATH}/${CI_JOB_ID}.json
      - ${JUNIT_REPORTS_PATH}/${CI_JOB_ID}.xml
      - ${CUCUMBER_REPORTS_PATH}/${CI_JOB_ID}.json
    reports:
      junit: ${JUNIT_REPORTS_PATH}/${CI_JOB_ID}.xml
  before_script:
    - npm ci --cache .npm --prefer-offline
  script:
    - $(npm bin)/cypress run --browser chrome --spec "${SPECS}"
  after_script:
    - mkdir --parents ${MOCHAWESOME_REPORTS_PATH} ${JUNIT_REPORTS_PATH} ${CUCUMBER_REPORTS_PATH}
    - $(npm bin)/mochawesome-merge ./cypress/results/mochawesome/*.json > ${MOCHAWESOME_REPORTS_PATH}/${CI_JOB_ID}.json
    - $(npm bin)/junit-merge ./cypress/results/junit/*.xml --out ${JUNIT_REPORTS_PATH}/${CI_JOB_ID}.xml
    - $(npm bin)/cucumber-json-merge ./cypress/results/cucumber-json/*.json --out ${CUCUMBER_REPORTS_PATH}/${CI_JOB_ID}.json

merge:
  stage: 📝 create reports
  image: node:lts-alpine
  artifacts:
    paths:
      - ${JUNIT_RESULTS_FILE}
      - ${JSON_RESULTS_FILE}
  when: always
  script:
    - npx junit-merge ${JUNIT_REPORTS_PATH}/*.xml --out ${JUNIT_RESULTS_FILE}
    - npx cucumber-json-merge ${CUCUMBER_REPORTS_PATH}/*.json --out ${JSON_RESULTS_FILE} || true

pages:
  stage: 📝 create reports
  image: node:lts-alpine
  artifacts:
    paths:
      - public
  when: always
  script:
    - find ${MOCHAWESOME_REPORTS_PATH}/ -type f -empty -delete
    - npx mochawesome-merge "./${MOCHAWESOME_REPORTS_PATH}/*.json" > mochawesome.json
    - npx mochawesome-report-generator --inline --showPassed false --showFailed true --reportDir public mochawesome.json
    - cp --recursive cypress/screenshots cypress/videos public/ || true

cucumber:
  stage: 📣 push results
  image: python:alpine
  when: always
  needs:
    - merge
  variables:
    OUTPUT_RESULTS_FILE: cleaned-test-results.json
  before_script:
    - apk add curl
    - python scripts/clean_cucumber_results.py
  script:
    - curl -X POST -F file=@./${OUTPUT_RESULTS_FILE} ${CUCUMBER_JSON_PUSH_URL}

tests:
  extends: .test
  stage: ✅ run tests
  variables:
    SPECS: cypress/e2e/**
